package com.codingpractices.foodprojectapp.ui.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.codingpractices.foodprojectapp.data.recipeslist.RecipesListRepository
import com.codingpractices.foodprojectapp.ui.recipeslist.RecipesListViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelFactory
@Inject
constructor(
    private val recipesListRepository: RecipesListRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            RecipesListViewModel::class.java -> {
                RecipesListViewModel(
                    recipesListRepository = recipesListRepository
                ) as T
            }

            else -> {
                throw IllegalArgumentException("unknown model class $modelClass")
            }
        }
    }
}
