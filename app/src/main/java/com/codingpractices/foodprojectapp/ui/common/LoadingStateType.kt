package com.codingpractices.foodprojectapp.ui.common

enum class LoadingStateType {
    LOADING,
    ACTIVE
}