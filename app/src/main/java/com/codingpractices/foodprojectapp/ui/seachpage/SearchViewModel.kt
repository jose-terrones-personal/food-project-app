package com.codingpractices.foodprojectapp.ui.seachpage

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import timber.log.Timber

class SearchViewModel: ViewModel() {
    init {
        Timber.i("SearchViewModel init")
    }
}

object SearchViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SearchViewModel() as T
    }
}