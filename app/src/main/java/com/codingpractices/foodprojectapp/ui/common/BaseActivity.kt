package com.codingpractices.foodprojectapp.ui.common

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    /**
     * TODO:
     * - Spinner logic
     * - DataBinding (Passing layout is from main)
     * - Nav Controller
     */
}