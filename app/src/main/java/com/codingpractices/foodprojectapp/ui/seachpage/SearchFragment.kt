package com.codingpractices.foodprojectapp.ui.seachpage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.databinding.FragmentSearchBinding
import com.codingpractices.foodprojectapp.ui.common.LoadingStateType
import com.codingpractices.foodprojectapp.ui.models.Title
import kotlinx.android.synthetic.main.custom_button.view.*

class SearchFragment : Fragment() {
    companion object {
        const val TITLE = "This is the search page"
    }

    private lateinit var binding: FragmentSearchBinding
    private val title = Title(TITLE)

    private val viewModel: SearchViewModel by viewModels { SearchViewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search,
            container,
            false
        )

        onPopulateLayout()
        binding.customButtonView.customButtonContainer.setOnClickListener {
            setSearchButtonListener(it)
        }
        return binding.root
    }

    private fun onPopulateLayout() {
        binding.title = title
        binding.customButtonView.setState(
            LoadingStateType.ACTIVE,
            resources.getString(R.string.search)
        )
    }

    private fun setSearchButtonListener(view: View) {
        view.findNavController().navigate(
            SearchFragmentDirections.actionSearchFragmentToRecipesListFragment(
                binding.searchField.text.toString()
            )
        )
    }
}