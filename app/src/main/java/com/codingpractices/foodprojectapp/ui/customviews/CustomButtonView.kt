package com.codingpractices.foodprojectapp.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.ui.common.LoadingStateType
import kotlinx.android.synthetic.main.custom_button.view.*

class CustomButtonView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null
) : ConstraintLayout(context, attributeSet) {

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_button, this, true)
    }


    fun setState(loadingState: LoadingStateType, action: String? = null) {
        when (loadingState) {
            LoadingStateType.ACTIVE -> {
                customButtonText.text = if(!action.isNullOrBlank()) action else "Button"
                customButtonProgressBar.visibility = View.GONE
            }
            LoadingStateType.LOADING -> {
                customButtonText.text = "Loading"
                customButtonProgressBar.visibility = View.VISIBLE
            }
        }
    }
}