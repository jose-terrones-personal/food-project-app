package com.codingpractices.foodprojectapp.ui.homepage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.databinding.FragmentHomeBinding
import com.codingpractices.foodprojectapp.ui.common.LoadingStateType
import kotlinx.android.synthetic.main.custom_button.view.*

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        onPopulateLayout()

        /**
         * Options Menu
         *
         * setHasOptionsMenu(true)
         */

        return binding.root
    }

    private fun onPopulateLayout() {
        /**
         * When the data is dynamically added:
         * Add invalidateAll() after setting the value so that the UI
         * is refreshed with the value in the updated binding object.
         */
        binding.customButtonView.setState(
            LoadingStateType.ACTIVE,
            resources.getString(R.string.start)
        )
        binding.customButtonView.customButtonContainer.setOnClickListener {
            onButtonClicked(it)
        }
    }

    private fun onButtonClicked(view: View) {
        view.findNavController()
            .navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment())
        binding.customButtonView.setState(LoadingStateType.LOADING)
    }

    /**
     * Options Menu
     *
     * override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
     *  super.onCreateOptionsMenu(menu, inflater)
     *  inflater.inflate(R.menu.options_menu, menu)
     * }
     *
     * override fun onOptionsItemSelected(item: MenuItem): Boolean {
     *  return NavigationUI.onNavDestinationSelected(
     *      item,
     *      view!!.findNavController()
     *  ) || super.onOptionsItemSelected(item)
     * }
     */

}