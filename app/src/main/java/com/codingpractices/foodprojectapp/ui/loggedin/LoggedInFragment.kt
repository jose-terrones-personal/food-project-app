package com.codingpractices.foodprojectapp.ui.loggedin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.ui.models.Title
import com.codingpractices.foodprojectapp.databinding.FragmentLoggedInBinding

/**
 * Logic for signIn should be on the listener
 * https://codelabs.developers.google.com/codelabs/kotlin-android-training-add-navigation/#5
 *
 * To change back button behaviour
 * https://codelabs.developers.google.com/codelabs/kotlin-android-training-add-navigation/#6
 */

class LoggedInFragment : Fragment() {
    private lateinit var binding: FragmentLoggedInBinding
    val title: Title = Title("This is the logged in page")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentLoggedInBinding>(
            inflater,
            R.layout.fragment_logged_in,
            container,
            false
        )
        binding.title = title
        return binding.root
    }
}