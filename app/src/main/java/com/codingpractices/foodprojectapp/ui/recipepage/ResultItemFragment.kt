package com.codingpractices.foodprojectapp.ui.recipepage

import androidx.fragment.app.Fragment

/**
 * Share the result with an external app
 * https://codelabs.developers.google.com/codelabs/kotlin-android-training-start-external-activity/#4
 * https://developer.android.com/training/basics/intents/sending
 */

class ResultItemFragment: Fragment() {
    /**
     * Call to recipe
     */
//    val serviceGenerator = ServiceGenerator()
//    val recipeCall = serviceGenerator.getRecipeApi.getRecipe(246916, API_KEY)
//    recipeCall.enqueue(object : Callback<RecipeResponse> {
//        override fun onFailure(call: Call<RecipeResponse>, t: Throwable) {
//            Timber.d(t.toString())
//        }
//
//        override fun onResponse(
//            call: Call<RecipeResponse>,
//            response: Response<RecipeResponse>
//        ) {
//            Timber.d(response.body().toString())
//        }
//    })
}