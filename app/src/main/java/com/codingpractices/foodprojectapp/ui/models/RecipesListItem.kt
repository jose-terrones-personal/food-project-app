package com.codingpractices.foodprojectapp.ui.models

data class RecipesListItem(
    var title: String? = "",
    var imageUrl: String? = ""
)