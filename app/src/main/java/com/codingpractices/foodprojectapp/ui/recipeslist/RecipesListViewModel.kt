package com.codingpractices.foodprojectapp.ui.recipeslist

import androidx.lifecycle.*
import com.codingpractices.foodprojectapp.data.recipeslist.RecipesListRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipesListViewModel
@Inject
constructor(
    private val recipesListRepository: RecipesListRepository
) : ViewModel() {
    
    val recipes = recipesListRepository.recipes
    val recipesResponseInfo = recipesListRepository.recipesResponseInfo
    val isQueryExhausted = recipesListRepository.isQueryExhausted

    // TODO: Create a data type with error message
    private val _isCallFailed: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val isCallFailed: LiveData<Boolean> = _isCallFailed

    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    fun getRecipes(key: String) = launchDataLoad {
        recipesListRepository.getRecipes(key)
    }

    fun getRecipesNextBatch(key: String) = launchDataLoad {
        if (!isQueryExhausted.value!!) {
            recipesListRepository.getRecipesNextBatch(key)
        }
    }

    private fun launchDataLoad(block: suspend () -> Unit) {
        viewModelScope.launch {
            try {
                _isLoading.value = true
                block()
            } catch (error: Throwable) {
                _isLoading.value = false
                // TODO: Create a data error type to pass the error message to the UI
                _isCallFailed.value = true
            } finally {
                _isLoading.value = false
                _isCallFailed.value = false
            }
        }
    }
}

