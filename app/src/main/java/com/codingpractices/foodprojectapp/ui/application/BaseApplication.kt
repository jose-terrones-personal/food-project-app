package com.codingpractices.foodprojectapp.ui.application

import android.app.Application
import com.codingpractices.foodprojectapp.di.ApplicationComponent
import com.codingpractices.foodprojectapp.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

/**
 * This is call in the manifest. Adding Timber
 * here will allow the whole app to use it.
 */
class BaseApplication : Application() {
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        initApplicationComponent()
        initLogger()
    }

    private fun initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.factory().create(this)
    }

    private fun initLogger() {
        Timber.plant(Timber.DebugTree())
    }
}