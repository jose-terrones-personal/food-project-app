package com.codingpractices.foodprojectapp.ui.recipeslist

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.data.network.models.ResultsItem
import com.codingpractices.foodprojectapp.databinding.LayoutRecipesListItemBinding
import com.codingpractices.foodprojectapp.ui.models.RecipesListItem

class RecipesListAdapter(
    private val context: Context,
    private val onRecipeListener: OnRecipeListener
) : RecyclerView.Adapter<RecipesListViewHolder>() {
    private lateinit var binding: LayoutRecipesListItemBinding
    private lateinit var recipes: List<ResultsItem?>
    private var imageBaseUrl: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesListViewHolder {
        val layoutInflater = LayoutInflater.from(context)

        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.layout_recipes_list_item,
            parent,
            false
        )

        return RecipesListViewHolder(binding, onRecipeListener)
    }

    override fun getItemCount(): Int = if (isRecipeReady()) recipes.size else 0

    override fun getItemId(position: Int): Long {
        return recipes[position]?.id?.let {
            it.toLong()
        } ?: super.getItemId(position)
    }

    override fun onBindViewHolder(holder: RecipesListViewHolder, position: Int) {
        if (isRecipeReady()) {
            val recipe = recipes[position]
            val item = RecipesListItem(recipe?.title, "$imageBaseUrl${recipe?.image}")
            holder.bind(item)
        }
    }

    fun setRecipes(items: List<ResultsItem?>) {
        recipes = items
        notifyDataSetChanged()
    }

    fun setImageBaseUrl(url: String) {
        imageBaseUrl = url
    }

    private fun isRecipeReady(): Boolean = ::recipes.isInitialized
}

interface OnRecipeListener {
    fun onRecipeClicked(position: Int)
}