package com.codingpractices.foodprojectapp.ui.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.codingpractices.foodprojectapp.ui.recipeslist.RecipesListFragment
import javax.inject.Inject

class FragmentAppFactory
@Inject
constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String) =
        when(className) {
            RecipesListFragment::class.java.name -> {
                RecipesListFragment(viewModelFactory)
            }
            else -> super.instantiate(classLoader, className)
        }

}