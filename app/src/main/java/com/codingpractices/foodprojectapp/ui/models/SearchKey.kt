package com.codingpractices.foodprojectapp.ui.models

data class SearchKey(var searchKey: String = "")