package com.codingpractices.foodprojectapp.ui.common

import android.content.Context
import androidx.fragment.app.Fragment
import com.codingpractices.foodprojectapp.di.ApplicationComponent
import com.codingpractices.foodprojectapp.ui.application.BaseApplication

abstract class BaseFragment : Fragment() {
    abstract fun inject()

    override fun onAttach(context: Context) {
        inject()
        super.onAttach(context)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return activity?.run {
            (application as BaseApplication).applicationComponent
        } ?: throw Exception("Application component is null")
    }
}