package com.codingpractices.foodprojectapp.ui.recipeslist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.codingpractices.foodprojectapp.databinding.LayoutRecipesListItemBinding
import com.codingpractices.foodprojectapp.ui.models.RecipesListItem

class RecipesListViewHolder(
    private val binding: LayoutRecipesListItemBinding,
    private val onRecipeListener: OnRecipeListener
) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
    init {
        binding.cardView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onRecipeListener.onRecipeClicked(adapterPosition)
    }

    fun bind(item: RecipesListItem) {
        binding.apply {
            recipesListItem = item
        }
    }
}