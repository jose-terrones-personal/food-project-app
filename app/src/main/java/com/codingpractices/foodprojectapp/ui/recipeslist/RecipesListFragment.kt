package com.codingpractices.foodprojectapp.ui.recipeslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.codingpractices.foodprojectapp.R
import com.codingpractices.foodprojectapp.databinding.FragmentRecipesListBinding
import com.codingpractices.foodprojectapp.ui.common.BaseFragment
import com.codingpractices.foodprojectapp.ui.fragments.SearchResultsFragmentArgs
import com.codingpractices.foodprojectapp.ui.models.SearchKey
import com.codingpractices.foodprojectapp.utils.VerticalSpacingListItemDecorator
import com.codingpractices.foodprojectapp.utils.onScrollToEnd
import timber.log.Timber

class RecipesListFragment
constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : BaseFragment(), OnRecipeListener {
    private val TAG = RecipesListFragment::class.java

    private lateinit var binding: FragmentRecipesListBinding
    private lateinit var adapter: RecipesListAdapter
    private val viewModel: RecipesListViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapter = RecipesListAdapter(requireContext(), this)
        bindingAll(inflater, container)
        subscribeObservers()

        return binding.root
    }

    override fun inject() {
        getApplicationComponent().inject(this)
    }

    override fun onResume() {
        super.onResume()

        binding.searchKey?.let {
            viewModel.getRecipes(it.searchKey)
        }
    }

    override fun onRecipeClicked(position: Int) {}

    private fun bindingAll(inflater: LayoutInflater, container: ViewGroup?) {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_recipes_list, container, false)

        binding.rwRecipesList.layoutManager = LinearLayoutManager(context)
        adapter.setHasStableIds(true)
        binding.rwRecipesList.adapter = adapter

        val args = SearchResultsFragmentArgs.fromBundle(requireArguments())
        binding.searchKey = SearchKey(args.searchKey)

        // Set the LifecycleOwner to be able to observe LiveData objects
        binding.lifecycleOwner = this

        binding.rwRecipesList.onScrollToEnd(binding.rwRecipesList) {
            onScrollToEndListener()
        }

        // Remove and add proper styling
        binding.rwRecipesList.addItemDecoration(VerticalSpacingListItemDecorator(30))
    }

    private fun subscribeObservers() {
        viewModel.recipes.observe(viewLifecycleOwner) { recipesList ->
            Timber.i("$TAG: List search results updated:: $recipesList")
            recipesList?.let {
                adapter.setRecipes(it)
            }
        }

        viewModel.recipesResponseInfo.observe(viewLifecycleOwner) { info ->
            info?.baseUri?.let { url ->
                adapter.setImageBaseUrl(url)
            }
        }

        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            val viewState = if (isLoading) View.VISIBLE else View.GONE
            binding.loadingState = viewState
        }

        viewModel.isCallFailed.observe(viewLifecycleOwner) { isCallFailed ->
            if (isCallFailed) {
                // TODO: Implement error view and error message
                Timber.i("$TAG: API call failed")
            }
        }

        viewModel.isQueryExhausted.observe(viewLifecycleOwner) { isQueryExhausted ->
            if (isQueryExhausted) {
                // TODO: Implement View
                Timber.i("$TAG: No more recipes:: Query is exhausted")
            }
        }
    }

    private fun onScrollToEndListener() {
        binding.searchKey?.let {
            viewModel.getRecipesNextBatch(it.searchKey)
        }
    }

}