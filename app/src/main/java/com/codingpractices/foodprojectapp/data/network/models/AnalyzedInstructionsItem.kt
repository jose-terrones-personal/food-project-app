package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AnalyzedInstructionsItem(
	val name: String? = null,
	val steps: List<StepsItem?>? = null
) : Parcelable