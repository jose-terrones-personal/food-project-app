package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResultsItem(
	val readyInMinutes: Int? = null,
	val sourceUrl: String? = null,
	val image: String? = null,
	val servings: Int? = null,
	val openLicense: Int? = null,
	val id: Int? = null,
	val title: String? = null
) : Parcelable