package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecipesResponse(
	val number: Int? = null,
	val totalResults: Int? = null,
	val offset: Int? = null,
	val baseUri: String? = null,
	val isStale: Boolean? = null,
	val results: List<ResultsItem?>? = null
) : Parcelable