package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WinePairing(
	val productMatches: List<ProductMatchesItem?>? = null,
	val pairingText: String? = null,
	val pairedWines: List<String?>? = null
) : Parcelable