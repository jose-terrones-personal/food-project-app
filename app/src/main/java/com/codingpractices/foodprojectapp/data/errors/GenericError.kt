package com.codingpractices.foodprojectapp.data.errors

class GenericError(message: String, error: Throwable) : Throwable(message, error)