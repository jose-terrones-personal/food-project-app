package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Length(
	val number: Int? = null,
	val unit: String? = null
) : Parcelable