package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductMatchesItem(
	val score: Double? = null,
	val price: String? = null,
	val imageUrl: String? = null,
	val averageRating: Double? = null,
	val link: String? = null,
	val description: String? = null,
	val id: Int? = null,
	val title: String? = null,
	val ratingCount: Double? = null
) : Parcelable