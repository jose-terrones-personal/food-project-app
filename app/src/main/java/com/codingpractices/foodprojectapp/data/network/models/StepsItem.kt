package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StepsItem(
	val number: Int? = null,
	val ingredients: List<IngredientsItem?>? = null,
	val equipment: List<EquipmentItem?>? = null,
	val step: String? = null,
	val length: Length? = null
) : Parcelable