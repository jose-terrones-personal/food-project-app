package com.codingpractices.foodprojectapp.data.network

import com.codingpractices.foodprojectapp.utils.API_KEY
import com.codingpractices.foodprojectapp.utils.BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RecipesService {
    private val interceptor = Interceptor { chain ->
        val url = chain.request()
            .url()
            .newBuilder()
            .addEncodedQueryParameter("apiKey", API_KEY)
            .build()
        val request = chain.request()
            .newBuilder()
            .url(url)
            .build()
        return@Interceptor chain.proceed(request)
    }

    /**
     * Add connectivity interceptor here
     * https://www.youtube.com/watch?v=0Nzl2MyaGxU
     */
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    private val retrofitBuilder by lazy {
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }

    private val recipesService by lazy {
        retrofitBuilder
            .build()
            .create(RecipesApiService::class.java)
    }

    val getRecipesService = recipesService
}