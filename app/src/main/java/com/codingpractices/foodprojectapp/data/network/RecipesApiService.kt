package com.codingpractices.foodprojectapp.data.network

import com.codingpractices.foodprojectapp.data.network.models.RecipeResponse
import com.codingpractices.foodprojectapp.data.network.models.RecipesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

// https://www.youtube.com/watch?v=RSYTn-O3r34
interface RecipesApiService {

    // RECIPES
    @GET("recipes/search")
    suspend fun getRecipes(
        @Query("query") key: String,
        @Query("number") number: Int,
        @Query("offset") offset: Int?
    ): RecipesResponse

    // RECIPE
    @GET("recipes/{id}/information")
    suspend fun getRecipe(
        @Path("id") id: Long
    ): RecipeResponse
}