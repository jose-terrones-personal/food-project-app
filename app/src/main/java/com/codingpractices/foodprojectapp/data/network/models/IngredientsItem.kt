package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IngredientsItem(
	val image: String? = null,
	val name: String? = null,
	val id: Int? = null
) : Parcelable