package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExtendedIngredientsItem(
	val image: String? = null,
	val amount: Double? = null,
	val original: String? = null,
	val aisle: String? = null,
	val consistency: String? = null,
	val originalName: String? = null,
	val unit: String? = null,
	val measures: Measures? = null,
	val meta: List<String?>? = null,
	val name: String? = null,
	val originalString: String? = null,
	val id: Int? = null,
	val metaInformation: List<String?>? = null
) : Parcelable