package com.codingpractices.foodprojectapp.data.recipeslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.codingpractices.foodprojectapp.data.errors.GenericError
import com.codingpractices.foodprojectapp.data.network.RecipesService
import com.codingpractices.foodprojectapp.data.network.models.RecipesResponseInfo
import com.codingpractices.foodprojectapp.data.network.models.ResultsItem
import com.codingpractices.foodprojectapp.utils.NETWORK_TIMEOUT
import com.codingpractices.foodprojectapp.utils.mergeLists
import kotlinx.coroutines.withTimeout
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecipesListRepository
@Inject
constructor(
    private val recipesService: RecipesService
) : Repository {

//    fun getRecipes(key: String): LiveData<List<ResultsItem?>?> {
//        /**
//         *  - Create a cache service and inject with Dagger
//         *  - https://developer.android.com/jetpack/docs/guide#cache-data
//         *
//         * val cached : LiveData<List<ResultsItem?>?> = recipesCache.get(key)
//         * if (cached != null) {
//         *   return cached
//         * }
//         */
//
//    }

    private val _recipes: MutableLiveData<List<ResultsItem?>?> by lazy {
        MutableLiveData<List<ResultsItem?>?>()
    }
    val recipes: LiveData<List<ResultsItem?>?>
        get() = _recipes

    private val _recipesResponseInfo: MutableLiveData<RecipesResponseInfo?> by lazy {
        MutableLiveData<RecipesResponseInfo?>()
    }
    val recipesResponseInfo
        get() = _recipesResponseInfo

    private val _isQueryExhausted: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val isQueryExhausted
        get() = _isQueryExhausted

    private var prevKey: String? = null
    private var totalRecipes: Int? = 0
    private var recipesShown: Int = 0

    override suspend fun getRecipes(key: String) {
        resetRecipesData(key)
        networkCall(key)
    }

    // TODO: offset shouldn't be hardcoded.
    //  Same value than returnNumber
    override suspend fun getRecipesNextBatch(key: String) {
        isNextBatch {
            networkCall(key, offset = 10)
        }
    }

    private fun resetRecipesData(key: String) {
        if (prevKey != key) {
            _recipes.value = null
            _recipesResponseInfo.value = null
            _isQueryExhausted.value = false
            prevKey = key
            recipesShown = 0
        }
    }

    // TODO: Move this to a data source file.
    //  Add programmatically the returnNumber.
    private suspend fun networkCall(key: String, returnNumber: Int = 10, offset: Int? = 0) {
        try {
            withTimeout(NETWORK_TIMEOUT) {
                recipesService.getRecipesService.getRecipes(key, returnNumber, offset)
            }.let { response ->
                response.results?.let { list ->
                    _recipes.value?.let {
                        _recipes.value = mergeLists(it, list)
                    } ?: _recipes.postValue(list)
                }

                _recipesResponseInfo.value =
                    RecipesResponseInfo(response.totalResults, response.baseUri)
                totalRecipes = response.totalResults
            }
        } catch (error: Throwable) {
            _recipes.postValue(null)
            throw GenericError("Unable to get recipes", error)
        }
    }

    private suspend fun isNextBatch(call: suspend () -> Unit) {
        totalRecipes
        if (recipesShown <= totalRecipes!!) {
            call()
            recipesShown += 10
        } else {
            _isQueryExhausted.value = true
        }
    }

}


// Rename this and add it to a different file (impl)
interface Repository {
    suspend fun getRecipes(key: String)
    suspend fun getRecipesNextBatch(key: String)
}

