package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecipesResponseInfo(
    val totalResults: Int? = null,
    val baseUri: String? = null
) : Parcelable