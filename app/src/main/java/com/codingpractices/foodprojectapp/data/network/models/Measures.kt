package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Measures(
	val metric: Metric? = null,
	val us: Us? = null
) : Parcelable