package com.codingpractices.foodprojectapp.data.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Metric(
	val amount: Double? = null,
	val unitShort: String? = null,
	val unitLong: String? = null
) : Parcelable