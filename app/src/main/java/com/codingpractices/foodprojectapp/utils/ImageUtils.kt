package com.codingpractices.foodprojectapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

// https://developer.android.com/topic/libraries/data-binding/binding-adapters

// TODO: Create a call interface and add loading state
@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}

/**
 * TODO: Add fallback image
 */
//@BindingAdapter("imageUrl", "error")
//fun loadImage(view: ImageView, url: String, error: Drawable) {
//    Picasso.get().load(url).error(error).into(view)
//}