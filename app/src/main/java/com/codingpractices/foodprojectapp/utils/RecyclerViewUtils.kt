package com.codingpractices.foodprojectapp.utils

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.onScrollToEnd(
    list: RecyclerView,
    onScrollEnd: (Unit) -> Unit
) = addOnScrollListener(object : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (!list.canScrollVertically(1)) {
            onScrollEnd(Unit)
        }
    }
})