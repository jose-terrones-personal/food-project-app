package com.codingpractices.foodprojectapp.utils

const val BASE_URL = "https://api.spoonacular.com/"
const val API_KEY = "a209dd1984bd42e48cc1728f79ff0f63"

const val DEFAULT_API_ITEMS_RETURN_NUMBER = 10

const val NETWORK_TIMEOUT: Long = 10000 // 10 sec