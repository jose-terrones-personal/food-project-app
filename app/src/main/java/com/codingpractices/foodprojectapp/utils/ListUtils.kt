package com.codingpractices.foodprojectapp.utils

fun <T> mergeLists(first: List<T>, second: List<T>): List<T> {
    return first.plus(second)
}