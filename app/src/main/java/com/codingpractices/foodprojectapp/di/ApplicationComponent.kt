package com.codingpractices.foodprojectapp.di

import com.codingpractices.foodprojectapp.ui.MainActivity
import com.codingpractices.foodprojectapp.ui.application.BaseApplication
import com.codingpractices.foodprojectapp.ui.recipeslist.RecipesListFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ViewModelFactoryModule::class,
        FragmentAppFactoryModule::class
    ]
)
interface ApplicationComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance app: BaseApplication): ApplicationComponent
    }

    fun inject(mainActivity: MainActivity)

    fun inject(recipesListFragment: RecipesListFragment)
}