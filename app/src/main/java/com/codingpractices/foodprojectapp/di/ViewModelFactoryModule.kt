package com.codingpractices.foodprojectapp.di

import androidx.lifecycle.ViewModelProvider
import com.codingpractices.foodprojectapp.data.recipeslist.RecipesListRepository
import com.codingpractices.foodprojectapp.ui.common.ViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ViewModelFactoryModule {

    @Singleton
    @Provides
    fun provideViewModuleFactory(
        recipesListRepository: RecipesListRepository
    ): ViewModelProvider.Factory {
        return ViewModelFactory(
            recipesListRepository = recipesListRepository
        )
    }
}