package com.codingpractices.foodprojectapp.di

import com.codingpractices.foodprojectapp.data.network.RecipesService
import com.codingpractices.foodprojectapp.data.recipeslist.RecipesListRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ApplicationModule {

    @Singleton
    @Provides
    fun provideRecipesService() = RecipesService

    @Singleton
    @Provides
    fun provideRecipesListRepository(
        recipesService: RecipesService
    ): RecipesListRepository {
        return RecipesListRepository(
            recipesService
        )
    }
}