package com.codingpractices.foodprojectapp.di

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.codingpractices.foodprojectapp.ui.common.FragmentAppFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object FragmentAppFactoryModule {

    @Singleton
    @Provides
    fun provideFragmentAppFactory(
        viewModelFactory: ViewModelProvider.Factory
    ): FragmentFactory {
        return FragmentAppFactory(viewModelFactory)
    }
}