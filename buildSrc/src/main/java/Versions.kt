object Versions {
    val gradle = "3.6.3"
    val kotlin = "1.3.71"
    val appcompat = "1.1.0"
    val ktx = "1.3.0"
    val viewmodel_savedstate = "1.0.0-alpha02"
    val constraintlayout = "1.1.3"
    val compileSdkVersion = 29
    val buildToolsVersion = "29.0.3"
    val minSdkVersion = 15
    val targetSdkVersion = 29
    val lifecycle_version = "2.2.0"
    val materialdesign_version = "1.2.0-beta01"
    val timber_version = "4.7.1"
    val leakcanary_version = "2.3"
    val retrofit_version = "2.9.0"
    val navigation = "2.3.0-beta01"
    val navigation_safe_args = "1.0.0-rc02"
    val picasso_version = "2.71828"
    val junit_4_version = "4.13"
    val espresso = "3.2.0"
    val androidx_test_ext = "1.1.1"
    val coroutines_version = "1.3.7"
    val dagger ="2.28.1"
}