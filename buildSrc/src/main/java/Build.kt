object Build {
    val build_tools = "com.android.tools.build:gradle:${Versions.gradle}"
    val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    val navigation_safe_args = "android.arch.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation_safe_args}"
}
