package dependencies

object TestDependencies {
    val junit4 = "junit:junit:${Versions.junit_4_version}"
}