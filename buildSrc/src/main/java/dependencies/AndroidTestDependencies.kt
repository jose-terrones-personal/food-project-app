package dependencies

object AndroidTestDependencies {
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    val androidx_test_ext = "androidx.test.ext:junit-ktx:${Versions.androidx_test_ext}"

    val instrumentation_runner = "androidx.test.runner.AndroidJUnitRunner"
}